import { Request, Response, NextFunction } from "express";
import Token from '../classes/token';

export const checkToken = (req: any, res: Response, next: NextFunction) => {
    const token = req.get('x-token') || '';

    Token.checkToken(token).then(
        (data: any) => {
            // console.log("Decoded", data);
            req.user = data.user;
            next();
        })
        .catch(err => {
            res.json({
                ok: false,
                msg: "Token falso"
            })
        });
}
import jwt from "jsonwebtoken";
/**
 * Clase estática
 */
export default class Token {

    private static seed: string = "semilla-o-k-ase";
    private static time: string = "30d";

    constructor() { }

    /**
     * 
     * @param payload 
     */
    static getJwtToken(payload: any): string {
        return jwt.sign({
            user: payload
        }, this.seed, { expiresIn: this.time });
    }

    static checkToken(token: string) {
        return new Promise((resolve, reject) => {

            jwt.verify(token, this.seed, (err, decoded) => {
                if (err) {
                    // no confiar
                    reject(err);
                }
                else {
                    resolve(decoded);
                }
            })
        });
    }

}
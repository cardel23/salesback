
export const Utils = {

    cleanArray: (arr: String[]) => {
        if (arr !== undefined) {
            arr.forEach((element, index) => {
                if (Object.is(element, "")) arr.splice(index, 1);
            });
            return arr;
        }
        else return [];
    },
    inRegex: (arr: String[]) => {
        var newArray: String[] = [];
        var joint;
        arr.forEach((element, index) => {
            newArray.push(`.*${element}.*`);
        });
        joint = newArray.join("|");

        return joint;
    }

}


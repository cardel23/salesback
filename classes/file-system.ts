import { FileUpload } from '../interfaces/file-upload';
import path from 'path';
import fs from 'fs';
import uniqid from 'uniqid';

export default class FileSystem {
    constructor() { };

    /**
     * Devuelve una promesa para ser manejada
     * en los posts
     * @param file 
     * @param userId 
     */
    saveTempImg(file: FileUpload, userId: string) {
        return new Promise((resolve, reject) => {
            try {


                // crear carpeta
                const path = this.createUserFolder(userId);

                // nombre de archivo
                const fileName = this.createFileName(file.name);

                // mover el archivo a la carpeta del usuario
                file.mv(`${path}/${fileName}`, (err: any) => {
                    if (err) {
                        console.log(err);
                        reject(err);
                    } else {
                        // todo bien
                        console.log("Creado ̈́".concat(fileName).concat(" en temp"));
                        resolve(fileName);
                    }
                });
            } catch (err) {
                console.log(err);
                reject(err);
            }
        });
    }
    /**
     * Guardar imagen base64
     * @param b64 
     * @param userId 
     */
    saveTempb64(b64: string, userId: string) {
        const path = this.createUserFolder(userId);
        const fileName = uniqid() + ".png";

        try {
            fs.writeFileSync(`${path}/${fileName}`,b64,'base64');
            return fileName;
        } catch (error) {
            throw error;
        }
    }

    saveUserImg(file: FileUpload, userId: string) {
        return new Promise((resolve, reject) => {

            // crear carpeta
            const path = this.createUserFolder(userId).replace('/temp', '');

            // nombre de archivo
            const fileName = this.createFileName(file.name);
            const exists = fs.existsSync(`${path}/avatar`);
            if (!exists)
                try {
                    fs.mkdirSync(`${path}/avatar`);
                } catch (error) {
                }
            // mover el archivo a la carpeta del usuario
            file.mv(`${path}/avatar/${fileName}`, (err: any) => {
                if (err) {
                    reject(err);
                } else {
                    // todo bien
                    resolve(fileName);
                }
            });
        });
    }

    private createFileName(originalFileName: string) {
        const arrName = originalFileName.split('.');
        const ext = arrName[arrName.length - 1];

        const uid = uniqid();

        return `${uid}.${ext}`;
    }
    

    private createUserFolder(userId: string) {

        const userPath = path.resolve(__dirname, '../uploads', userId);
        const userTempPath = userPath + '/temp';
        console.log(userPath);

        const exists = fs.existsSync(userPath);

        if (!exists) {
            fs.mkdirSync(userPath);
        }
        if (!fs.existsSync(userTempPath))
            fs.mkdirSync(userTempPath);

        return userTempPath;
    }

    tempToPostImg(userId: string) {
        const tempPath = path.resolve(__dirname, '../uploads', userId, 'temp');
        const postsPath = path.resolve(__dirname, '../uploads', userId, 'posts');

        if (!fs.existsSync(tempPath)) {
            console.log('No existe la ruta ' + tempPath);
            return [];
        }

        if (!fs.existsSync(postsPath)) {
            fs.mkdirSync(postsPath);
            console.log("Creado el directorio " + postsPath);
        }

        const tempImages = this.getTempImages(userId);

        tempImages.forEach(img => {
            console.log("Moviendo " + img + " desde " + tempPath + " hacia " + postsPath);
            fs.renameSync(`${tempPath}/${img}`, `${postsPath}/${img}`);
        });

        return tempImages;
    }

    private getTempImages(userId: string) {
        const tempPath = path.resolve(__dirname, '../uploads', userId, 'temp');

        return fs.readdirSync(tempPath) || [];
    }

    getImgUrl(userId: string, dir: string, img: string) {
        if (img === undefined) {
            return path.resolve(__dirname, '../uploads/shapes.svg');
        }
        const imgUrl = path.resolve(__dirname, '../uploads', userId, dir, img);

        const exists = fs.existsSync(imgUrl);
        if (!exists) {
            return path.resolve(__dirname, '../uploads/shapes.svg');
        }
        return imgUrl;
    }

    getUserImgUrl(userId: string, img: string) {
        const imgUrl = path.resolve(__dirname, '../uploads', userId, 'avatar', img);

        const exists = fs.existsSync(imgUrl);
        if (!exists) {
            return path.resolve(__dirname, '../uploads/default.png');
        }
        return imgUrl;
    }
}
import { Schema, model, Document } from "mongoose";
import { User } from './usuario.model';
import { Response } from 'express';

const revSchema = new Schema({
    body: {
        type: String,
        required: [true]
    },
    created: Date,
    rating: {
        type: Number
    },
    ratedUser: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
    ratedBy: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    response: {
        type: String
    }
});

revSchema.pre<IReview>('save', function(next) {
    this.created = new Date();
    next();
});

interface IReview extends Document {
    body: string;
    created: Date;
    rating: number;
    ratedUser: string;
    ratedBy: string;
    response: string;
};

export const Review = model<IReview>('Review', revSchema);
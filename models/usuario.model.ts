import { Schema, model, Document } from "mongoose";
import bcrypt from 'bcrypt';

const userSchema = new Schema({
    name: {
        type: String,
        required: [true, "Este campo name es obligatorio"]
    },
    avatar: {
        type: String,
        default: 'av-1.png'
    },
    email: {
        type: String,
        unique: true,
        required: [true, "El campo email es obligatorio"]
    },
    pass: {
        type: String,
        required: [true, "El campo pass es obligatorio"]
    },
    products: {
        type: Number,
        default: 0
    },
    reviews: {
        type: Number,
        default: 0
    },
    rating: {
        type: Number,
        default: 0
    },
    favs: Array
});

// necesitamos funcion normal para hacer referencia a this
userSchema.method("passCompare", function(pass: string = ""):boolean{
    if(bcrypt.compareSync(pass, this.pass)){
        return true;
    } else return false;
})

interface IUser extends Document {
    name: string,
    email: string,
    pass: string,
    avatar: string,
    products: number
    reviews: number;
    rating: number;
    favs: string[];

    passCompare(pass: string): boolean;
}

export const User = model<IUser>('User', userSchema);
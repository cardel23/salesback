import { Schema, model, Document } from "mongoose";

const itemSchema = new Schema({
    name: {
        type: String,
        required: [true, "El campo es requerido"]
    },
    description: String,
    created: Date,
    price: Number,
    img: Array,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: [true, 'Debe existir referencia a user']
    },
    type: String,
    status: String,
    discount: Number,
    keywords: Array,
    swap: {type: Boolean, default: false},
    favs: Array,
    details: String
});

itemSchema.pre<IItem>('save', function(next) {
    this.created = new Date();
    next();
});

interface IItem extends Document {
    name: string;
    description: string;
    price: number;
    created: Date;
    img: string[];
    user: string;
    type: string;
    status: string;
    discount: number;
    keywords: string[];
    swap: boolean;
    favs: string[];
    details: string;

};

export const Item = model<IItem>('Item', itemSchema);
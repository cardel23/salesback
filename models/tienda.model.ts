import { Schema, model, Document } from "mongoose";

const tiendaSchema = new Schema({
    nombre: {
        type:String,
        required: [true,"Campo obligatorio"],
        unique: true
    },
    descripcion: String,
    direccion: String,
    img: String,
    categoria: String,
    coords: String
});

interface ITienda extends Document {
    nombre: string,
    descripcion: string,
    direccion: string,
    img: string,
    categoria: string,
    coords: string
};

export const Tienda = model<ITienda>('Tienda',tiendaSchema);
import { Router } from "express";
import { ProductController } from '../controller/product.controller';
import { checkToken } from '../middlewares/auth';
import { UserController } from "../controller/user.controller";
import { ReviewController } from '../controller/review.controller';
import { Review } from '../models/review.model';

const rutas = Router();

/* PRODUCTOS */
rutas.get('/item', ProductController.obtenerItems);
rutas.get('/item/:id', ProductController.obtenerItem);
// rutas.get('/item/:id/:user', ProductController.obtenerItems);
rutas.get('/item/prueba', ProductController.pruebaItem);
rutas.post('/item/nuevo',checkToken, ProductController.guardar);
rutas.post('/item/update',checkToken, ProductController.update);
rutas.post('/item/imagen/guardar', checkToken, ProductController.guardarImagen);
rutas.get('/img/:userId/:dir/:img?', ProductController.obtenerImagen);

/* USUARIOS */
rutas.post('/usuario/crear', UserController.guardarUsuario);
rutas.get('/usuario', UserController.obtenerUsuarios);
rutas.get('/usuario/:id', UserController.obtenerUsuario);
// rutas.get('/tienda', Controller.obtenerTiendas);
// rutas.post('/tienda/crear', Controller.guardarTienda);
rutas.post('/usuario/imagen/guardar', checkToken, UserController.guardarImagenUsuario);
rutas.get('/usuario/img', checkToken, UserController.obtenerImagenUsuario);
rutas.post('/usuario/login', UserController.login);
rutas.put('/usuario/fav', UserController.actualizarFavoritos);

/* REVIEWS */
rutas.get('/review/:uid', ReviewController.getReviews);
rutas.post('/review', ReviewController.create);
rutas.put('/review/:id', ReviewController.update);
export default rutas;
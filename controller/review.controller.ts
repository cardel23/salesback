import { Item } from '../models/item.model';
import { User } from '../models/usuario.model';
import { Query } from "mongoose";
import { Utils } from '../classes/util';
import { Response } from 'express';
import { Review } from '../models/review.model';

const controller = {

    getReviews: async (req: any, res: Response) => {
        var user = req.params.uid;
        let page = Number(req.query.page) || 1;
        let skip = page - 1;
        skip *= 10;
        var data = await Review.find({ ratedUser: user })
            .skip(skip)
            .limit(10)
            .populate("ratedUser", ['-pass', '-__v'])
            .populate("ratedBy", ['-pass', '-__v'])
            .sort({ "created": "-1" })
            .exec();

        if (data) {
            res.json({
                ok: true,
                data
            });
        } else
            res.json({
                ok: false,
                data: []
            });
    },
    create: (req: any, res: Response) => {
        var params = req.body;
        var rev: any = {
            body: params.body,
            ratedBy: params.ratedBy,
            ratedUser: params.ratedUser,
            rating: params.rating
        };
        Review.create(rev).then(async revDB => {
            let data = revDB.toJSON();
            try {
                await updateReviews(data.ratedUser);
                var rev = await Review.findById(data._id)
                    .populate("ratedUser", ['-pass', '-__v'])
                    .populate("ratedBy", ['-pass', '-__v'])
                    .exec();
                return res.json({
                    ok: true,
                    data: rev
                });
            } catch (error) {
                return res.json({
                    ok: false,
                    error
                });
            }

        }).catch(error => {
            return res.json({
                ok: false,
                error
            });
        });
    },
    /**
     * 
     */
    update: async (req: any, res: Response) => {
        var response = req.body.response;
        var id = req.params.id;
        Review.findByIdAndUpdate(id, { response: response }, { new: true }, (err, data) => {
            if (err)
                res.json({
                    ok: false,
                    error: err,
                    msg: "Se ha producido un error"
                });
            if (!data) {
                return res.json({
                    ok: false,
                    msg: "No existe el id"
                });
            }
            res.json({
                ok: true,
                data
            });
        }).populate("ratedUser", ['-pass', '-__v']).populate("ratedBy", ['-pass', '-__v']);
    }

};

async function updateReviews(uid: string) {
    var data = await countReviews(uid);
    try {
        var upd = await User.findByIdAndUpdate(uid, { reviews: data.reviews, rating: data.avg }).exec();
        // console.log("update", upd);
    } catch (error) { }
}

async function countReviews(uid: string) {
    var array = await Review.find({ ratedUser: uid }).exec();
    var avg = 0;
    array.forEach((item) => {
        avg += item.rating;
    });
    var data = {
        reviews: array.length,
        avg: avg / array.length
    }

    return data;

}

export const ReviewController = controller;
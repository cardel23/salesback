import { Router, Request, Response } from "express";
import { Item } from '../models/item.model';
import { User } from '../models/usuario.model';
import { Tienda } from '../models/tienda.model';
import { FileUpload } from '../interfaces/file-upload';
import FileSystem from '../classes/file-system';
import Token from '../classes/token';
import bcrypt from "bcrypt";
import { Query } from "mongoose";
import { Utils } from '../classes/util';

const fileSystem = new FileSystem();

const controller = {
    pruebaItem: (req: Request, res: Response) => {
        return res.json({
            message: 'Ola k ase'
        });
    },
    guardar: (req: any, res: Response) => {
        try {
            var params = req.body;
            var art: any = {
                name: params.name,
                description: params.description,
                price: params.price,
                img: params.img,
                user: req.user._id,
                type: params.type,
                discount: params.discount,
                keywords: params.keywords || [],
                status: params.status,
                details: params.details
            };

            console.log(JSON.stringify(art));

            Item.create(art).then(async itemDB => {
                // data.udsuario = req.user;
                let data = itemDB.toJSON();
                data.user = req.user;
                fileSystem.tempToPostImg(req.user._id);
                // console.log(data);
                var docs = await contar(req.user);
                req.user.products = docs;
                console.log(req.user);
                var uq = await User.findByIdAndUpdate(req.user._id, req.user).exec();
                // console.log(uq);
                return res.json({
                    msg: "Ok",
                    data
                })
            }).catch(error => {
                console.log(error);
                return res.json({
                    msg: "Error",
                    error
                })
            })
        } catch (error) {
            return res.json({
                msg: "Error",
                error: "Error de autenticación"
            });
        }
    },
    update: async (req: any, res: Response) => {

        var item = req.body;

        var data = await Item.findByIdAndUpdate(item._id, item, { new: true }).populate("user", ["-pass", "-__v"]).exec();
        if (!data) {
            return res.json({
                ok: false,
                msg: "No hay usuario"
            });
        }
        console.log(data);
        res.json({
            ok: true,
            data
        });
    },
    obtenerItems: async (req: any, res: Response) => {

        let page = Number(req.query.page) || 1;
        let skip = page - 1;
        skip *= 10;
        var excludePid = req.query.expid;
        var pid = (!Object.is(req.query.pid, undefined)) ? Utils.cleanArray(req.query.pid.split(",")) : [];
        var user = req.query.userId;
        var keywords = (!Object.is(req.query.keywords, undefined)) ? Utils.cleanArray(req.query.keywords.split(",")) : [];
        var orKeywords = req.query.orKeywords;
        var type = (!Object.is(req.query.type, undefined)) ? Utils.cleanArray(req.query.type.split(",")) : [];
        var orType = req.query.orType;
        var minPrice = req.query.minPrice;
        var maxPrice = req.query.maxPrice;
        var status = (!Object.is(req.query.status, undefined)) ? Utils.cleanArray(req.query.status.split(",")) : [];
        var sort = (!Object.is(req.query.sort, undefined)) ? Utils.cleanArray(req.query.sort.split(",")) : [];
        var search = req.query.search;
        var fav = req.query.fav;
        var consulta = new Query();
        if (excludePid)
            consulta.where("_id", { $ne: excludePid });
        // consulta.and("user").equals(user);
        if (pid.length > 0)
            consulta.where("_id").in(pid);
        if (fav)
            consulta.where("favs").in(fav);
        if (user)
            consulta.where("user", user);
        if (keywords.length > 0)
            consulta.where('keywords').in(keywords);
        if (type.length > 0)
            consulta.where("type").in(type);
        if (status.length > 0)
            consulta.where("status").in(status);
        if (minPrice)
            consulta.where("price").gte(minPrice);
        if (maxPrice)
            consulta.where("price").lte(maxPrice);
        if (search) {
            if (search.length > 0) {
                var regex = Utils.inRegex(search.split(","));
                consulta.or([
                    { name: { $regex: regex, $options: "i" } },
                    { description: { $regex: regex, $options: "i" } },
                    { details: { $regex: regex, $options: "i" } },
                    { keywords: { $regex: regex, $options: "i" } },
                    { type: { $regex: regex, $options: "i" } },
                ]);
            }
        }
        if (orKeywords) {
            if (orKeywords.length > 0) {
                var regex = Utils.inRegex(orKeywords.split(","));
                consulta.or([
                    { keywords: { $regex: regex, $options: "i" } }
                ]);
            }
        }
        if (orType) {
            if (orType.length > 0) {
                var regex = Utils.inRegex(orType.split(","));
                consulta.or([
                    { type: { $regex: regex, $options: "i" } }
                ])
            }
        }
        if (sort.length > 0) {
            // consulta.sort(["-price"]);
            var sortArray: Array<Array<string>> = [];
            sort.forEach((item, index) => {
                var order;
                if (item.charAt(0) === "-") {
                    item = item.substring(1);
                    order = "-1";
                } else {
                    order = "1";
                }
                sortArray.push([item.toString(), order]);
            });
            consulta.sort(sortArray);
        } else {
            consulta.sort({ created: -1 });
        }

        const items = await Item.find(consulta)
            // .sort({ created: -1 })
            .skip(skip)
            .limit(10)
            .populate('user', ['-pass', '-__v'])
            .exec();
        // console.log(items);
        res.json({
            ok: true,
            page: page,
            results: items.length,
            items,
        });
    },
    obtenerItem: async (req: any, res: Response) => {
        const params = req.params;
        try {
            var result = await Item.findById(params.id)
                .populate("user", ['-pass', '-__v'])
                .exec();
            return res.json({
                ok: true,
                data: result
            })
        } catch (error) {
            return res.status(404).send({
                ok: false,
                data: "No se encontro el articulo"
            });
        }
    },
    guardarTienda: (req: Request, res: Response) => {
        var params = req.body;
        var user: any = {
            name: params.name,
            description: params.description,
            direccion: params.direccion,
            img: params.img,
            categoria: params.categoria,
            coords: params.coords
        }
        Tienda.create(user).then(userDB => {
            return res.json({
                status: "ok",
                userDB
            })
        }).catch(error => {
            return res.json({
                status: 'error',
                error
            })
        });
    },
    obtenerTiendas: (req: Request, res: Response) => {
        Tienda.find({}).then(userDB => {
            return res.json({
                status: "ok",
                userDB
            })
        }).catch(error => {
            return res.json({
                status: "error",
                error
            })
        })
    },
    guardarImagen: (req: any, res: Response) => {
        if (req.body.img) {
            let img = req.body.img;
            let file = fileSystem.saveTempb64(img, req.user._id);
            if (file.includes("png")) {
                return res.json({
                    response: {
                        ok: true,
                        file
                    }
                });
            }
            else {
                return res.json({
                    response: {
                        ok: false,
                        msg: "No se subio nada"
                    }
                });
            }
        }

        if (!req.files) {
            console.log("No se subio nada")
            return res.status(400).json({
                ok: false,
                msg: "No se subio nada"
            });
        }

        const file: FileUpload = req.files.img;

        if (!file) {
            console.log("No se subio ninguna imagen");
            return res.status(400).json({
                ok: false,
                msg: "No se subio ninguna imagen"
            });
        }

        if (!file.mimetype.includes('image')) {
            console.log("Esto no es una imagen")
            return res.status(400).json({
                ok: false,
                msg: "Esto no es una imagen"
            });
        }

        fileSystem.saveTempImg(file, req.user._id).then(data => {
            if (data) {
                return res.status(200).json({
                    ok: true,
                    file: data
                });
            }
        }).catch(error => {
            console.log(error);
            return res.json({
                ok: false,
                error
            })
        })
    },
    obtenerImagen: (req: any, res: Response) => {
        const userId = req.params.userId;
        const img = req.params.img;
        const dir = req.params.dir;
        const imgPath = fileSystem.getImgUrl(userId, dir, img);
        res.sendFile(imgPath);
        // },
        // contar: async function(user?:any){
        //     var consulta = new Query();

        //     if(user){
        //         consulta.where("user", user);
        //     }
        //     var result = await Item.count(consulta).exec();
        //     return result;
    }
}


async function contar(user?: any) {
    var consulta = new Query();

    if (user) {
        consulta.where("user", user);
    }
    var result = await Item.count(consulta).exec();
    console.log("Resultado", result);
    return result;
}


export const ProductController = controller;
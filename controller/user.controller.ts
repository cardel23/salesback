import { Router, Request, Response } from "express";
import { Item } from '../models/item.model';
import { User } from '../models/usuario.model';
import { Tienda } from '../models/tienda.model';
import { FileUpload } from '../interfaces/file-upload';
import FileSystem from '../classes/file-system';
import Token from '../classes/token';
import bcrypt from "bcrypt";
import { Query } from "mongoose";

const fileSystem = new FileSystem();


const controller = {

    login: (req: Request, res: Response) => {
        const body: any = req.body;

        User.findOne({ email: body.email }, (err, data) => {
            if (err) throw err;
            if (!data) {
                return res.json({
                    ok: false,
                    msg: "Usuario/Contraseña incorrecta"
                });
            }
            if (data.passCompare(body.pass)) {

                const token = Token.getJwtToken({
                    _id: data._id,
                    name: data.name,
                    // pass: data.pass,
                    email: data.email,
                    avatar: data.avatar
                });

                res.json({
                    ok: true,
                    msg: token
                });
            } else {
                return res.json({
                    ok: false,
                    msg: "Usuario/Contraseña Incorrecta"
                });
            }
        })
    },
    guardarUsuario: (req: Request, res: Response) => {
        var params = req.body;
        var user: any = {
            name: params.name,
            email: params.email,
            pass: bcrypt.hashSync(params.pass, 10)
        }
        User.create(user).then(userDB => {
            return res.json({
                status: "ok",
                userDB
            })
        }).catch(error => {
            return res.json({
                status: 'error',
                error
            })
        });
    },
    actualizarUsuario: (req: any, res: Response) => {

        var user: any = {
            name: req.body.name || req.user.name,
            email: req.body.email || req.user.email,
            // pass: bcrypt.hashSync(req.body.pass, 10) || req.user.pass,
            avatar: req.body.avatar || req.user.avatar
        };

        User.findByIdAndUpdate(req.user._id, user, { new: true }, (err, data) => {
            if (err) throw err;
            if (!data) {
                return res.json({
                    ok: false,
                    msg: "No hay usuario"
                });
            }
            const token = Token.getJwtToken({
                _id: data._id,
                name: data.name,
                email: data.email,
                avatar: data.avatar
            });
            res.json({
                ok: true,
                token: token
            });
        });
    },
    obtenerUsuarios: (req: Request, res: Response) => {
        User.find({}).then(userDB => {
            return res.json({
                ok: true,
                userDB
            })
        }).catch(error => {
            return res.json({
                ok: false,
                error
            })
        })
    },
    obtenerUsuario: async (req: any, res: Response) => {
        var id = req.params.id
        try {
            var query = await User.findById(id).select(["-pass","-__v"]).exec();
            return res.json({
                ok: true,
                user: query
            })
        } catch (error) {
            return res.json({
                ok:false,
                error
            })
        }
    },
    obtenerImagenUsuario: (req: any, res: Response) => {
        const userId = req.user._id;
        const img = req.user.avatar;
        const imgPath = fileSystem.getUserImgUrl(userId, img);
        res.sendFile(imgPath);
    },
    guardarImagenUsuario: (req: any, res: Response) => {
        if (!req.files) {
            return res.status(400).json({
                ok: false,
                msg: "No se subio nada"
            });
        }

        const file: FileUpload = req.files.img;

        if (!file) {
            return res.status(400).json({
                ok: false,
                msg: "No se subio ninguna imagen"
            });
        }

        if (!file.mimetype.includes('image')) {
            return res.status(400).json({
                ok: false,
                msg: "Esto no es una imagen"
            });
        }

        fileSystem.saveUserImg(file, req.user._id).then(img => {
            if (img) {
                var user: any = {
                    name: req.body.name || req.user.name,
                    email: req.body.email || req.user.email,
                    // pass: bcrypt.hashSync(req.body.pass, 10) || req.user.pass,
                    avatar: img || req.user.avatar
                };

                User.findByIdAndUpdate(req.user._id, user, { new: true }, (err, data) => {
                    if (err) throw err;
                    if (!data) {
                        return res.json({
                            ok: false,
                            msg: "No hay usuario"
                        });
                    }
                    const token = Token.getJwtToken({
                        _id: data._id,
                        name: data.name,
                        email: data.email,
                        avatar: data.avatar
                    });
                    return res.status(200).json({
                        ok: true,
                        file: img,
                        token
                    });
                });
            }
            
        })
    },
    actualizarFavoritos:(req:any, res:Response) => {
        User.findByIdAndUpdate(req.body._id, req.body, { new: true }, (err, data) => {
            if (err) throw err;
            if (!data) {
                return res.json({
                    ok: false,
                    msg: "No hay usuario"
                });
            }
            res.json({
                ok: true,
                data
            });
        });     
    }
}

export const UserController = controller;
import bodyParser from "body-parser";
import mongoose from 'mongoose';
import Server from './classes/server';
import rutas from './routes/routes';
import fileupload from "express-fileupload";
import cors from 'cors';

const server = new Server();

const app = server.app;


// middleware bodyparser
app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));

// carga de archivos
// opcional ({useTempFiles: true})
app.use(fileupload());

// cors
app.use(cors({ origin: true, credentials: true }));
// app.use((req, res, next) => {
//     res.header('Access-Control-Allow-Origin', '*');
//     res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
//     res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
//     res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
//     next();
// });

// rutas
app.use('/api', rutas);
// app.use('/posts', postRoutes);

// conectar db
mongoose.connect('mongodb://localhost:27017/ventas', {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
}, (err) => { if (err) throw err; console.log("Base de datos conectada") });

// iniciar express
server.listen(() => {
    console.log(`Servidor corriendo en puerto ${server.port}`)
});